class PackMan {

    constructor(x, y, r) {
        this.initX = x;
        this.initY = y;
        this.r = r;
        this.startAngle = radians(1);
        this.endAngle = PI/4;
        this.pacSpeed = 1;
        this.eyeR = 12;
        this.theta = radians(1);
        this.currentX = 0;
        this.currentY = 0;
        this.currentAngle = this.startAngle;
        this.isReached = false;
        this.offset = 1.3;
        this.maroonR = 100;
        this.maroonG = 20;
        this.maroonB = 19;
        this.eyeYPos = 20;
        this.packEyePos = this.currentY;
        //this.total = 0;
        this.score = 0;

    }

    draw(angle, distance)
    {
        //Checking whether the packman has reached to the rectangle or not
        if(this.currentX >= distance)
        {
            this.score = 10;
            this.offset = 0;
            this.theta = 0;
            this.isReached = true;
        }
        push();
        translate(this.initX, this.initY);
        rotate(angle);
        // mouth Movement
        noStroke();
        this.currentAngle+=this.theta;
        if(this.currentAngle< this.startAngle || this.currentAngle> this.endAngle){
            this.theta = - this.theta;
            this.currentAngle+=this.theta;
        }

        // Draw :::: Packman
        fill(this.maroonR,this.maroonG, this.maroonB);
        arc(this.currentX, this.currentY, this.r, this.r, this.currentAngle, -this.currentAngle,PIE);
        this.currentX += this.offset;
        // draw eyes

        if(angle > PI/2 && angle < TWO_PI-PI/2)
        {
            this.packEyePos = this.currentY + this.eyeYPos;
        }
        else {
            this.packEyePos = this.currentY - this.eyeYPos;
        }
        fill(255);
        ellipse(this.currentX, this.packEyePos, this.eyeR, this.eyeR);
        //draw eyeball

        fill(0)
        ellipse(this.currentX, this.packEyePos, this.eyeR-4, this.eyeR-4);
        pop();
    }

}
