class Rectangle
{
    constructor(x, y, wid, h)
    {
        this.x = x;
        this.y = y;
        this.wid = wid;
        this.h = h;
    }

    draw()
    {
        rect(this.x, this.y, this.wid, this.h);
    }
}
