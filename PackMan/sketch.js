var xOfRect;
var yOfRect;
var widthOfRect = 30;
var heightOfRect = 20;
var packMan;
var rectangle;
var angle = 0;
var pacXPos;
var pacYPos;
var pacRadius = 80;
var angle = 0;
var oppSideDiff = 0;
var baseSideDiff = 0;
var distance = 0;
var score = 0;


function setup()
{
    createCanvas(windowWidth, windowHeight);
    pacXPos = windowWidth/2;
    pacYPos = windowHeight/2;
    rectMode(CENTER);
    xOfRect = random(50, width - 100);
    yOfRect = random(80, height - 80);
    rectangle = new Rectangle(xOfRect, yOfRect, widthOfRect, heightOfRect);
    packMan = new PackMan(pacXPos, pacYPos , pacRadius);
    distance = dist(xOfRect,yOfRect,pacXPos,pacYPos);
}

function draw()
{
    // Score Board ::: Boundary
    background(128, 160, 153);
    textSize(20);
    textStyle(ITALIC);
    text("Scores: "+score,50,20);
    text("PackMan Game",width/2,20);
    line(0, 39, width, 39);
    stroke(100);
    strokeWeight(2);
    rectangle.draw();
    oppSideDiff = yOfRect- pacYPos;
    baseSideDiff = xOfRect - pacXPos;
    angle = atan(oppSideDiff/baseSideDiff);
    if(baseSideDiff < 0){
        angle += PI;
    }
    packMan.draw(angle, distance);
    if(packMan.isReached)
    {
        score += packMan.score;
        reachedAtTarget();
    }

}

function reachedAtTarget()
{
    pacXPos = int(xOfRect);
    pacYPos = int(yOfRect);
    xOfRect = random(50, width - 100);
    yOfRect = random(40, height - 80);
    rectangle = new Rectangle(xOfRect, yOfRect, widthOfRect, heightOfRect);
    packMan = new PackMan(pacXPos, pacYPos , pacRadius);
    packMan.isReached = false;
    distance = dist(xOfRect,yOfRect,pacXPos,pacYPos);

}
