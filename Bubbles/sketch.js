let bubbles = [];

var col = {
  r: 255,
  g: 0,
  b: 0
};


function setup() {//Thing that happens first and once
   createCanvas(800,400);
   //One way
   for(let i = 0; i < 10; i++)
   {
     let xAxis = random(width);
     let yAxis = random(height);
     let radius = random(10, 40);
     bubbles[i] = new Bubble(xAxis, yAxis, radius);
   }
}
//2nd Way
// function mousePressed()
// {
//   let radius = random(10,50);
//   let bubblessss = new Bubble(mouseX, mouseY, radius);
//   //bubbles.push(bubblessss);
//   bubbles.pull(bubblessss);
//   fill(col.r,col.g,col.b,100);
// }
//3rd Way
function mouseDragged()
{
  let radius = random(10,50);
  let bubblessss = new Bubble(mouseX, mouseY, radius);
  bubbles.push(bubblessss);

 }

//4th way
function mousePressed()
{
//  bubbles.clicked();
  // for(let i = 0; i < bubbles.length; i++)
  // {
  //   //bubbles[i].clicked();
  //   bubbles[i].move();
  //   bubbles[i].show();
  // }
  //remove bubble
  //for(let i = 0; i < bubbles.length; i++)
  for(let i = bubbles.length-1; i >= 0; i--)
  {
    if(bubbles[i].clicked())
    {
      bubbles.splice(i, 1);//remove an element from array
    }
  }
}

function draw() {//loops
    background(0);
    for(let i = 0; i < bubbles.length; i++)
    {
      if(bubbles[i].clicked())
      {
        bubbles[i].changeColor(255);
      }else {
        bubbles[i].changeColor(0);
      }
      //bubbles[i].clicked();
      bubbles[i].move();
      bubbles[i].show();
    }
    //snake like structure
    if(bubbles.length > 10)
    {
      bubbles.splice(0,1);
    }

    col.r = random(100,255);
    col.g = 0;
    col.b = random(100,190);
    //fill(col.r,col.g,col.b,100);

}
class Bubble {
  constructor(xAxis, yAxis, radius)
  {
    this.xAxis = xAxis;
    this.yAxis = yAxis;
    this.radius = radius;
    this.color = 0;
  }
  changeColor(color)
  {
    this.color = color;
  }
  clicked()
  {
    let distance = dist(mouseX, mouseY, this.xAxis, this.yAxis);
    if(distance < this.radius)
    {
      //this.color = 189;
      return true;
      //console.log("Clicked on Bubble");
    }else  {
      //this.color = 0;
      return false;
    }
  }
  move()
  {
    this.xAxis += random(-5, 5);
    this.yAxis += random(-5, 5);
  }
  show()
  {
     stroke(255);
     strokeWeight(4);
     fill(this.color, 125);
     ellipse(this.xAxis,this.yAxis,this.radius * 2);
  }
}
