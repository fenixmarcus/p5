var easing = 1;
var x = 0;

function setup()
{
    createCanvas(1000, 500);
    background(250);
}

function draw()
{
    var targetX = mouseX;
    x += (targetX - x)*easing;
    ellipse(x, 40, 12, 12);
    print(targetX+ " "+x);
}
