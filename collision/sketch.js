var bubbles = [];

function setup() //Thing that happens first and once
{
   createCanvas(800,400);
   for(var i = 0; i < 4;i++)
   {
     bubbles[i] = new Bubble(random(width),random(height));
   }
   //bubbles[0] = new Bubble(250,200);
   //bubbles[1] = new Bubble(350,200);
}

function draw() //loops
{
  background(0);
  for(var i = 0; i < bubbles.length; i++)
  {
    bubbles[i].update();
    bubbles[i].display();
    for(var j = 0; j < bubbles.length; j++)
    {
      if(i != j && bubbles[i].intersect(bubbles[j]))
      {
        bubbles[i].changeColor();
        bubbles[j].changeColor();
      }
    }
   // bubbles[0].update();
  // bubbles[1].update();
  // bubbles[0].display();
  // bubbles[1].display();

  // var distance = dist(bubbleOne.x,bubbleOne.y,bubbleTwo.x,bubbleTwo.y)
  //
  // if(distance < bubbleOne.radius + bubbleTwo.radius)
  // {
  //   bubbleOne.changeColor();
  //   bubbleTwo.changeColor();
  // }
  // if(bubbles[0].intersect(bubbles[1]))
  // {
  //   bubbles[0].changeColor();
  //   bubbles[1].changeColor();
  // }

}
}
function Bubble(x, y)
{
  this.x = x;
  this.y = y;
  this.radius = 24;
  this.col = color(255);

  this.changeColor = function()
  {
    this.col = color(random(255),0,random(255));
  }
  this.display = function()
  {
    stroke(255);
    fill(this.col);
    ellipse(this.x, this.y, this.radius * 2, this.radius *2);
  }
  this.update = function()
  {
    this.x += random(-1, 1);
    this.y += random(-1, 1);
  }
  this.intersect = function(other)
  {
    var distance = dist(this.x, this.y, other.x, other.y);

    if(distance < this.radius + other.radius)
    {
      return true;
    }
    else {
      return false;
    }
  }
}
