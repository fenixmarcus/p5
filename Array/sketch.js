var words = ["Rainbow","Shine","Colors","Heart","Nature","Beauty"];
var colors = [156,243,232,111,12,178];
var lengthOfColors = colors.length;
var lengthOfWords = words.length;
var index = 0;
var colorIndex = 243;
function setup() {//Thing that happens first and once
   createCanvas(800,400);
}

function draw() {//loops
    background(0);
    fill(colorIndex);
    textSize(32);
    text(words[index],12,200);
}
function mousePressed()
{
    index += 1;
    colors += 1;
    if(index == lengthOfWords - 1 || colorIndex == lengthOfColors - 1)
    {
      index = 0;
      colorIndex = 189;
    }
}
