let bubble;

function setup() {//T hing that happens first and once
   createCanvas(800,400);
   bubble = new Bubble();
}
// var x = 0;
// var speed = 3;

function draw() {//loops
  //rect(x-axis,y-axis,width,height)
  //fill(),stroke(),background()functions should always above
//1 example:basic

   //background(150,150,150);//can add 4th params(transparency) also
  // ellipseMode(CENTER);
  // rectMode(CENTER);
  // //Body
  // fill(255,0,0);
  // rect(240,145,20,100);
  //
  // //Head
  // fill(0,0,255);
  // ellipse(240,115,60,60);
  //
  // //Eyes
  // fill(0,255,0);
  // ellipse(221,115,16,32);
  // ellipse(259,115,16,32);
  //
  // //Legs
  // line(230,195,220,205);
  // line(250,195,260,205);

//2 example:
  //ellipse
  //background(150,150,150);
  //noStroke();
//  fill(250,200,200);
  //ellipse(mouseX,mouseY,50,50);
  //rectangle
  //fill(200,250,250);
  //rect(400,100,50,50);

//3 example:object()
  // var circlePosX = 0;
  // var circlePosY = 0;
  // var circleSize = 1;


  // var circle = { //Object
  //   circlePosX : 0,
  //   circlePosY : 0,
  //   circleSize : 2
  // }

  //background(150,150,150);
  //ellipse
  // fill(250,200,200);
  // ellipse(circle.circlePosX,circle.circlePosY,circle.circleSize,circle.circleSize);
  // circle.circlePosX += 1;
  // circle.circlePosY += 1;
  // circle.circleSize += 0.1;

  // var circlePosX = 0;
  // var circlePosY = 0;
  // var circleSize = 1;

//4 example:map()
  // r = map(mouseX,0,600,0,255);
  // b = map(mouseX,0,600,0,255);
  // background(r, 0, b);
  // fill(250,118,222);
  // ellipse(mouseX,200,64,64);

//5 example: random()

  // var r = 0;
  // var g = 0;
  // var b = 255;

  // var spot = {
  //   x: 100,
  //   y: 50
  // },
  // var col = {
  //   r: 255,
  //   g: 0,
  //   b: 0
  // };
  // col.r = random(100,255);
  // col.g = 0;
  // col.b = random(100,190);
  // spot.x = random(0,width);
  // spot.y = random(0,height);
  // noStroke();
  // fill(col.r,col.g,col.b,100);
  // ellipse(spot.x,spot.y,24,24);

//6 example: conditional statements
  // background(0);
  // stroke(255);
  // strokeWeight(4);
  // noFill();
  // if(mouseX > 300)
  // {
  //   fill(50,8,222);
  // }
  //
  // ellipse(300,200,100,100);

//7 example:Bounsing ball

  // background(0);
  // stroke(255);
  // strokeWeight(4);
  // noFill();
  // ellipse(x,200,50,50);
  //
  // if(x > width || x < 0)
  // {
  //   speed = speed*-1;
  // }
  // x = x + speed;

//8 example: Boolean variables;
//   background(0);
//var booleanValue = true;
//   if(booleanValue)
//   {
//     background(190,255,0);
//   }else
//   {
//     background(0);
//   }
//   stroke(255);
//   strokeWeight(4);
//   noFill();
//   if(mouseX > 250 && mouseX < 350)
//   {
//     fill(140,5,60);
//   }
//   rectMode(CENTER);
//   rect(300,200,50,50);
// }
//   function mousePressed(){
//     if(mouseX > 250 && mouseX < 350){
//       if(booleanValue){
//         booleanValue = false;
//       }else{
//         booleanValue = true;
//       }
//     }
//   }

//9 example:loops
    // var xAxis = 0;
    // var yAxis = 0;
    // var smallCirAxis = 0;
    // background(0);
    // strokeWeight(4);
    // stroke(250);

    // while(xAxis <= width)
    // {
    //   fill(0,200,255);
    //   ellipse(xAxis,200,25,25);
    //   xAxis += 50;
    // }
    // for(xAxis = 0; xAxis <= width; xAxis += 50)
    // {
    //   fill(140,5,60);
    //   ellipse(xAxis,100,40,40);
    //   for(smallCirAxis = 0; smallCirAxis <= width; smallCirAxis += 50)
    //   {
    //     fill(0,200,255);
    //     ellipse(smallCirAxis,100,15,15);
    //   }
    // }
    //
      //grid
    // for(xAxis = 0; xAxis <= width; xAxis += 50)
    // {
    //   for(yAxis = 0; yAxis <= height; yAxis += 50)
    //   {
    //     fill(random(140),0,random(60));
    //     ellipse(xAxis,yAxis,40,40);
    //   }
    // }
    //
      //grid but on mouseX and mouseY
    // for(xAxis = 0; xAxis <= mouseX; xAxis += 50)
    // {
    //   for(yAxis = 0; yAxis <= mouseY; yAxis += 50)
    //   {
    //     fill(random(140),0,random(60));
    //     ellipse(xAxis,yAxis,40,40);
    //   }
    // }
      //x and y axis plane
    // for(xAxis = 0; xAxis <= width; xAxis += 50)
    // {
    //   fill(random(140),0,random(60));
    //   ellipse(xAxis,200,40,40);
    // }
    // for(yAxis = 0; yAxis <= height; yAxis += 50)
    // {
    //   fill(random(140),0,random(60));
    //   ellipse(200,yAxis,40,40);
    // }

//10 example: Bounsing ball randomly
    //   background(0);
    //   move();
    //   bounce();
    //   display();
    // }
    // var ball = {
    //   xAxis: 300,
    //   yAxis: 200,
    //   xAxisSpeed: 4,
    //   yAxisSpeed: -3
    // }
    // function move()
    // {
    //   ball.xAxis += ball.xAxisSpeed;
    //   ball.yAxis += ball.yAxisSpeed;
    // }
    //
    // function bounce()
    // {
    //   if(ball.xAxis > width || ball.xAxis < 0)
    //   {
    //     ball.xAxisSpeed *= -1;
    //   }
    //   if(ball.yAxis > height || ball.yAxis < 0)
    //   {
    //     ball.yAxisSpeed *= -1;
    //   }
    //
    // }
    //
    // function display()
    // {
    //   strokeWeight(4);
    //   stroke(250);
    //   noFill();
    //   ellipse(ball.xAxis,ball.yAxis,24,24);
    // }//var booleanValue = true;
// classes in JS    background(0);
    background(0);
    bubble.move();
    bubble.show();

    }
    class Bubble {
      constructor()
      {
        this.xAxis = 200;
        this.yAxis = 150;
      }
      move()
      {
        this.xAxis += random(-5, 5);
        this.yAxis += random(-5, 5);
      }
      show()
      {
         stroke(255);
         strokeWeight(4);
         //noStroke();
         noFill();
         ellipse(this.xAxis,this.yAxis,24,24);
      }

    }
