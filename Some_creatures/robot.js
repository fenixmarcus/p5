var speed = 1;
function setup()
{
    createCanvas(800, 600);
    background(204);
}

function draw()
{
    strokeWeight(2);

    line(220, 120, 200, 80);
    line(250, 115, 270, 40);
    line(300, 150, 320, 155);
    ellipse(250, 150, 100, 100);//head
    ellipse(230, 130, 15, 15);//first EYe
    ellipse(230, 130, 4, 4);//eyeball Of First Eye
    ellipse(270, 130, 15, 15);//second eye
    ellipse(270, 130, 4, 4);//eyeball of second eye
    arc(250, 150, 50, 50, 0, PI);//Smile
    line(235, 197, 235, 250);
    line(245, 200, 245, 250);
    line(255, 200, 255, 250);
    line(265, 197, 265, 250);
    rect(200, 250, 100, 150);
    arc(250, 400, 80, 80, 0, PI, PIE);
}

function move()
{

}
