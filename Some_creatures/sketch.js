function setup()
{
    createCanvas(600, 400);
    background(204);
}

function draw()
{

    beginShape();
    vertex(100, 140);
    vertex(135, 105);
    vertex(135, 130);
    vertex(200, 130);
    vertex(200, 160);
    vertex(135, 160);
    vertex(135, 185);
    endShape(CLOSE);

}
