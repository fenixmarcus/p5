function setup()
{
    createCanvas(1000, 600);
    fill(255);
    stroke(102);
}

function draw()
{
    background(255);
    for(var x = 20; y < width-20; x += 10)
    {
        for(var y = 20; x < height-20; y += 10)
        {
            ellipse(x, y, 5, 5);
            line(x, y, width/2, height/2);
        }
    }
}
