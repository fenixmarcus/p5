function setup()
{
    createCanvas(800, 600);
    background(204);
}

function draw()
{
    strokeWeight(10);
    ellipse(100, 100, 80, 80);
    strokeWeight(20);
    ellipse(200, 100, 80, 80);
}
