var sliders = [];
//var x = 118;
var angle = 0;

function setup()
{
  noCanvas();
  //createCanvas(200,200);
  for(var i = 0; i < 50; i++)
  {
    sliders[i] = createSlider(0, 255, 50);
  }

  sliders[0].input(adjustSliders);

}
function adjustSliders()
{
  var offset = 0;
  for(var i = 1; i < sliders.length; i++)
  {
    var x = map(sin(angle+offset), -1, 1, 0, 255);
    sliders[i].value(x);
    offset += 0.025;
  }

  // x += random(-5, 5);
  // background(slider.value());

  angle += 0.05;
}
