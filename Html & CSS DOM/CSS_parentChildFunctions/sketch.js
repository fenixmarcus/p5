var para;
var anchorTag;
var image;
var imageArray = [];
var clearButton;

function setup()
{
  noCanvas();
  for(var i = 0; i < 3; i++)
  {
    para = createP("This is a link:   ");
    para.style("background-color", "pink");
    para.style("padding", "20px");
    anchorTag = createA("#", " Show me coding train!!!");
    anchorTag.mousePressed(addPhoto);
    anchorTag.parent(para);
  }
  clearButton = select("#clear");
  clearButton.mousePressed(clearImages);
}
function clearImages()
{
  for(var i = 0; i < imageArray.length; i++)
  {
    imageArray[i].remove();
  }
}
function addPhoto()
{
  image = createImg("codingTrain.jpg");
  imageArray.push(image);
  image.size(120, 60);
  var paragraph = this.parent();
  image.parent(paragraph);
}
