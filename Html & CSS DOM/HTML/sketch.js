var canvas;
var h1;
var xAxis = 100;
var yAxis = 100;

function setup() {//T hing that happens first and once
   canvas = createCanvas(400,200);
   canvas.position(400,300);
   h1 = createElement("h1", "Click me");
   h1.position(400,300);
}
function mousePressed()
{
  h1.html("My created heading from dom");
  createP("A created para on mousePressed by using dom.");
}

function draw() {
  background(200);
  fill(255,0,0);
  rect(xAxis,yAxis,50,50);
  h1.position(xAxis, yAxis);
  xAxis += random(-2, 2);
}
