function setup()
{
  for(var i = 0; i < 10; i++)
  {
    var p = createP("Apples");
    var x = floor(random(windowWidth));
    var y = floor(random(windowHeight));
    p.position(x, y);
    p.class('apple');
  }

  for(var i = 0; i< 10; i++)
  {
    var p = createP("Oranges");
    var x = floor(random(windowWidth));
    var y = floor(random(windowHeight));
    p.position(x, y);
    p.class('orange');
    p.mousePressed(switchToApple);
  }
}

function switchToApple()
{
  this.removeClass('orange');
  this.class('apple');
}
function draw()
{

}
