var backColor;
var button;
var slider;
var nameInput;
var nameP;

function setup() {
  canvas = createCanvas(200,200);
  createElement("br");
  backColor = color(200);
  nameP = createP("Your Name!");
  button = createButton("Click");
  button.mousePressed(changeColor);
  slider = createSlider(10, 100, 15);
  nameInput = createInput("Type your name");

  nameP.mouseOver(overPara);
  nameP.mouseOut(outPara);
  //input.changed(updateText);
  nameInput.input(updateText);

}
function updateText()
{
  nameP.html(nameInput.value());
}
function overPara()
{
  nameP.html("your mouse is over me!!");
}
function outPara()
{
  nameP.html("your mouse is out!!")
}
function changeColor()
{
  backColor = color(random(118));
}

function draw() {
  background(backColor);
  fill(255,0,175);
  ellipse(100,100,slider.value(),slider.value());
  //nameP.html(input.value());
  text(input.value(), 10, 20);
}
