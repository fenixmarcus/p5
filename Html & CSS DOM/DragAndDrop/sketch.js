var dropFile;

function setup()
{
  var canvas = createCanvas(100, 100);
  background(0);
  // dropFile = select("#dropFile");
  // dropFile.dragOver(highlight);
  // dropFile.dragLeave(unHighlight);
  // dropFile.drop(gotFile, unHighlight);
  canvas.drop(gotFile);
}
function gotFile(file)
{
  createP(file.name);
  var img = createImg(file.data);
  img.size(20, 20);
  image(img, 0, 0, width, height);


}
// function highlight()
// {
//   dropFile.style("background-color", "pink");
// }
// function unHighlight() {
//   dropFile.style("background-color", "#fff");
// }
