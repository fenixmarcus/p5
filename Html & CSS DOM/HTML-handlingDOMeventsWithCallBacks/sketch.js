var backColor;
var button;
var xAxis = 100;
var yAxis = 100;

function setup() {
   canvas = createCanvas(400,200);
   backColor = color(200);
   button = createButton("Click Click");
   button.mousePressed(changeColor);
}
function changeColor()
{
  backColor = color(random(118));
}
function changePosition()
{
  xAxis += random(-2, 2);
  yAxis += random(-2, 2);
}
function draw() {
  background(backColor);
  fill(255,0,175);
  rect(xAxis,yAxis,50,50);
  changePosition();
}
