class Circle {
    constructor(x, y, r, fixed)
    {
        this.options = {
            friction: 0,
            restitution: 0.95,
            isStatic: fixed
        }
            this.body = Bodies.circle(x, y, r, this.options);
            this.r = r;
            World.add(world, this.body);
    }

    isOfScreen()
    {
        var position = this.body.position;
        return (position.y > height + 100)
    }

    removeFromWorld()
    {
        World.remove(world, this.body);
    }

    draw()
    {
        var position = this.body.position;
        var angle = this.body.angle;
        push();
        translate(position.x, position.y);
        rotate(angle);
        //ellipseMode(R);
        strokeWeight(1);
        stroke(10);
        //var c = rgba(255, 0, 0, 0.4);
        fill('rgba(255, 0, 0, 0.2)');
        ellipse(0, 0, this.r*2);
        line(0, 0, this.r, 0);
        pop();
    }
}
