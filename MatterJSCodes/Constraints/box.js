class Box {
    constructor(x, y, w, h, fixed)
    {
        this.options = {
            friction: 0.3,
            restitution: 1,//bounce
            isStatic: fixed
        }
        this.body = Bodies.rectangle(x, y, w, h, this.options);
        this.w = w;
        this.h = h;
        World.add(world, this.body);
    }

    isOfScreen()
    {
        var position = this.body.position;
        return (position.y > height + 100)
    }

    removeFromWorld()
    {
        World.remove(world, this.body);
    }

    draw()
    {
        var pos = this.body.position;//position
        var angle = this.body.angle;
        push();
        translate(pos.x, pos.y);
        rotate(angle);
        rectMode(CENTER);
        strokeWeight(1);
        noStroke();
        fill(0);
        rect(0, 0, this.w, this.h);
        pop();
    }
}
