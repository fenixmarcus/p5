class CircleBoundary {
    constructor(x, y, w, h, a)
    {
        this.options = {
            friction: 0,
            restitution: 1,//bounce
            //angle: a,
            isStatic: true
        }
        this.body = Bodies.rectangle(x, y, w, h, this.options);
        this.w = w;
        this.h = h;
        World.add(world, this.body);
    }

    show()
    {
        var pos = this.body.position;//position
        var angle = this.body.angle;
        push();
        translate(pos.x, pos.y);
        rotate(angle);
        rectMode(CENTER);
        strokeWeight(1);
        noStroke();
        fill(0);
        rect(0, 0, this.w, this.h);
        pop();
    }
}
