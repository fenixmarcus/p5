// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies,
    Constraint = Matter.Constraint,
    Mouse = Matter.Mouse,
    MouseConstraint = Matter.MouseConstraint;


var engine;
var world;
var boxes = [];
var shapes;
var options;
var ground;
var boundaries = [];
var constraint;
var mouseConstraint;
function setup()
{
    var canvas = createCanvas(800, 600);
    //create engine
    engine = Engine.create();
    world = engine.world;
    //Engine.run(engine);
    var prev = null;
    for(var x = 300; x < 480; x += 20)
    {
        var fixed = false;
        if(!prev)
        {
            fixed = true;
        }
        shapes = new Circle(x, 100, 25, fixed);
        boxes.push(shapes);
        if(prev)
        {
            options = {
                bodyA: shapes.body,
                bodyB: prev.body,
                length: 50,
                stiffness: 0.4
            }
            constraint = Constraint.create(options);
            World.add(world, constraint);
        }
        prev = shapes;
    }
    boundaries.push(new CircleBoundary(400, height-25, width, 50));

    console.log(canvas);
    var canvasEleM = Mouse.create(canvas.elt);
    canvasEleM.pixelRatio = pixelDensity();//for high density display
    var mouseOptions = {
        mouse: canvasEleM
    }

    mouseConstraint = MouseConstraint.create(engine, mouseOptions);
    World.add(world, mouseConstraint);
}

function draw()
{
    background(100);
    Engine.update(engine);
    for(var i = 0; i < boxes.length; i++)
    {
        boxes[i].draw();
        if(boxes[i].isOfScreen())
        {
            boxes[i].removeFromWorld();
            boxes.splice(i, 1);
            i--;//to get rid of flickering
        }
    }
    for(var i = 0; i < boundaries.length; i++)
    {
        boundaries[i].show();
    }

    if(mouseConstraint.body)
    {
        var pos = mouseConstraint.body.position;
        var mousePos = mouseConstraint.mouse.position;
        var offset = mouseConstraint.constraint.pointB;
        fill(0, 100,0);
        line(pos.x + offset.x, pos.y + offset.y, mousePos.x, mousePos.y);
    }

    //line(boxes[0].body.position.x, boxes[0].body.position.y, boxes[1].body.position.x, boxes[1].body.position.y)
}
