// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies;

var engine;
var world;
var boxes = [];
var shapes;

var ground;
var boundaries = [];
function setup()
{
    createCanvas(800, 400);
    //create engine
    engine = Engine.create();
    world = engine.world;
    //Engine.run(engine);
    options = {
        isStatic: true
    }
    ground = Bodies.rectangle(400, height, width, 50, options);
    // boundaries.push(new CircleBoundary(450, 300, width*0.4, 30, -0.3));
    World.add(world, ground);
}

function mouseDragged()
{
    shapes = new Box(mouseX, mouseY, 20, 20);
    //shapes = new Circle(mouseX, mouseY, 8);
    boxes.push(shapes);
}

function draw()
{
    background(100);
    Engine.update(engine);
    for(var i = 0; i < boxes.length; i++)
    {
        boxes[i].show();
        if(boxes[i].isOfScreen())
        {
            boxes[i].removeFromWorld();
            boxes.splice(i, 1);
            i--;//to get rid of flickering
        }
    }
    //ground.show();

    noStroke()
    fill(0);
    rectMode(CENTER);
    rect(400, height, width, 50);
}
