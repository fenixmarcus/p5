// module aliases
var Engine = Matter.Engine,
    Render = Matter.Render,
    World = Matter.World,
    Bodies = Matter.Bodies;

var engine;
var world;
var boxes = [];
var shapes;

var ground;
var boundaries = [];
function setup()
{
    createCanvas(800, 400);
    //create engine
    engine = Engine.create();
    world = engine.world;
    //Engine.run(engine);
    boundaries.push(new CircleBoundary(250, 150, width*0.4, 30, 0.3));
    boundaries.push(new CircleBoundary(450, 300, width*0.4, 30, -0.3));
    //World.add(world, boundaries);
}

// function mouseDragged()
// {
//     //shapes = new Box(mouseX, mouseY, 20, 20);
//     shapes = new Circle(mouseX, mouseY, 8);
//     boxes.push(shapes);
// }

function draw()
{
    background(100);
    boxes.push(new Circle(200, 50, 8));
    Engine.update(engine);
    for(var i = 0; i < boxes.length; i++)
    {
        boxes[i].draw();
        if(boxes[i].isOfScreen())
        {
            boxes[i].removeFromWorld();
            boxes.splice(i, 1);
            i--;//to get rid of flickering
        }
    }
    for(var i = 0; i < boundaries.length; i++)
    {
        boundaries[i].show();
    }

    // noStroke()
    // fill(170);
    // rectMode(CENTER);
    // rect(ground.position.x, ground.position.y, width, 50);
}
