class Circle {
    constructor(x, y, r)
    {
        this.options = {
            friction: 0,
            restitution: 0.5
        }
            this.body = Bodies.circle(x, y, r, this.options);
            this.r = r;
            World.add(world, this.body);
            //console.log(this.body);
    }

    isOfScreen()
    {
        var position = this.body.position;
        return (position.y > height + 100)
    }

    removeFromWorld()
    {
        World.remove(world, this.body);
    }

    draw()
    {
        var position = this.body.position;
        var angle = this.body.angle;
        push();
        translate(position.x, position.y);
        rotate(angle);
        strokeWeight(1);
        noStroke();
        fill(0);
        ellipse(0, 0, this.r*2);
        pop();
    }
}
