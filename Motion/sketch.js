// var radius = 40;
// var x = -radius;
// var speed = 0.5;
// function setup() {
//     createCanvas(240, 120);
//     ellipseMode(RADIUS);
// }
// function draw() {
//     background(0);
//     x += speed; // Increase the value of x
//     console.log("speed: "+x);
//     arc(x, 60, radius, radius, 0.52, 5.76);
// }

var bugs = [];
function setup() {
    createCanvas(240, 120);
    background(204);
    for (var i = 0; i < 33; i++) {
    var x = random(width);
    var y = random(height);
    var r = i + 2;
    bugs[i] = new JitterBug(x, y, r);
    }
}
function draw() {
    for (var i = 0; i < bugs.length; i++) {
    bugs[i].move();
    bugs[i].display();
    }
}

function JitterBug(tempX, tempY, tempDiameter) {
    this.x = tempX;
    this.y = tempY;
    this.diameter = tempDiameter;
    this.speed = 2.5;
    this.move = function() {
        this.x += random(-this.speed, this.speed);
        this.y += random(-this.speed, this.speed);
    };
    this.display = function() {
        ellipse(this.x, this.y, this.diameter, this.diameter);
    };
}
