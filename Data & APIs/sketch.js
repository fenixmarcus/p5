var data;

function preload()
{
  data = loadJSON("data.json");
  console.log(data);
}

function setup()
{
    noCanvas();
    var flower = data.flower[1];
    createP(flower);
}
